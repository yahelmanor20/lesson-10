#include "Item.h"
Item::Item(string name, string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}
Item::~Item()
{
}

double Item::totalPrice() const
{
	return _count*_unitPrice;
}

string Item::serialNumber()
{
	return this->_serialNumber;
}

string Item::name()
{
	return this->_name;
}

void Item::printItem() const
{
	cout <<atoi(_serialNumber.c_str()) << " " << this->_name << " " << this->_unitPrice<<" * "<<this->_count<<endl;
}

int Item::count() const
{
	return this->_count;
}

void Item::setCount(int count)
{
	_count = count;
}

bool Item::operator<(const Item & other) const
{
	return _serialNumber < other._serialNumber? true:false;
}

bool Item::operator>(const Item & other) const
{
	return _serialNumber > other._serialNumber ? true : false;
}

bool Item::operator==(const Item & other) const
{
	return _serialNumber == other._serialNumber ? true : false;
}


