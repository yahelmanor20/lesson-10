#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::Customer()
{
}

double Customer::totalSum() const
{
	double count = 0;
	set<Item>::iterator iter;
	for (auto elem : _items)
	{
		count += elem.totalPrice();
	}
	return count;
}

void Customer::addItem(Item add)
{
	// remove if needed and replace it with higher count
	int count = _items.find(add)!=_items.end()? _items.find(add)->count():0;
	if (_items.count(add) >= 1)
	{
		_items.erase(add);
		add.setCount(count+1);
		_items.insert(add);
	}
	else
	{
		_items.insert(add);
	}
}

void Customer::removeItem(Item add)
{
	// remove if needed and replace it with lower count
	int count = _items.find(add) != _items.end() ? _items.find(add)->count() : 0;
	if (count>1)
	{
		_items.erase(add);
		add.setCount(count-1);
		_items.insert(add);
	}
	else
	{
		_items.erase(add);
	}
}

set<Item> Customer::items()
{
	return this->_items;
}
